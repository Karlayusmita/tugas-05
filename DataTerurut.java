import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.io.PrintWriter;

class barang{
	int kode,jumlah, harga;
	String jenisBarang, warna;
	
	public barang(int kode, String jenisBarang, String warna, int jumlah, int harga){
		this.kode = kode;
		this.jenisBarang = jenisBarang;
		this.warna = warna;
		this.jumlah = jumlah;
		this.harga = harga;
	}
	
	public int getKode(){
		return kode;
	}
	
	public String getJenisBarang(){
		return jenisBarang;
	}
	
	public String getWarna(){
		return warna;
	}
	
	public int getJumlah(){
		return jumlah;
	}
	
	public int getHarga(){
		return harga;
	}
}

class menulis{
	ArrayList<barang> tampung;
	
	public menulis(){
		tampung = new ArrayList<barang>();
	}
	
	public void isiData(int kode, String jenisBarang, String warna, int jumlah, int harga){
		tampung.add(new barang(kode, jenisBarang, warna,jumlah,harga));
	}
	
	public void lokasiFile(){
		
	}
	
	public void tampilkanData(String inputLokasiPenyimpanan){
		BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
		
			try{
				File filePenyimpanan = new File(inputLokasiPenyimpanan);
				FileWriter tulisFile = new FileWriter(filePenyimpanan.getAbsoluteFile());
				BufferedWriter bufferedwriter = new BufferedWriter(tulisFile);
				String baris = null;
				FileReader membacaFile = new FileReader(inputLokasiPenyimpanan);
				BufferedReader bufferedreader1 = new BufferedReader(membacaFile);
				try{
					if((baris = bufferedreader1.readLine()) != null) {
						try{
							for(barang Barang : tampung){
								StringBuffer stringbuffer = new StringBuffer(baris);
								String tulisKode = "Kode = "+ Barang.getKode();
								String tulisJenisBarang = "Jenis Barang = " + Barang.getJenisBarang();
								String tulisWarna = "Warna = "+ Barang.getWarna();
								String tulisJumlah = "Jumlah= "+ Barang.getJumlah();
								String tulisHarga = "Harga = "+ Barang.getHarga();
								String tulisBatas = "============================================================";
								String barisBaru = ""+stringbuffer.append(System.getProperty("line.separator"));
								
								bufferedwriter.write(tulisBatas);
								bufferedwriter.write(barisBaru);
								bufferedwriter.write(tulisKode);
								bufferedwriter.write(barisBaru);
								bufferedwriter.write(tulisJenisBarang);
								bufferedwriter.write(barisBaru);
								bufferedwriter.write(tulisWarna);
								bufferedwriter.write(barisBaru);
								bufferedwriter.write(tulisJumlah);
								bufferedwriter.write(barisBaru);
								bufferedwriter.write(tulisHarga);
								bufferedwriter.close();
							}
						}
						catch (Exception e){
							e.printStackTrace();
						}
					}
				}
				catch(FileNotFoundException ex) {
					System.out.println("Unable to open file '" + membacaFile + "'");
				}
				catch(IOException ex) {
					System.out.println("Error reading file '" + membacaFile + "'");
				}
			}
			catch(Exception e){
				System.out.println(e);
			}
	}
}

public class DataTerurut{
	static Object[][] arr=new Object[11][6];
	static void mainMenu(){
		System.out.println("Menu Utama");
		System.out.println("1. Input Data Barang");
		System.out.println("2. Tampilkan Data Barang");
		System.out.println("0. Keluar");
		System.out.print("Masukkan PIlihan Anda = ");
	}
	
	public static void main(String[] args){
		BufferedReader bufferedreader = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Masukkan lokasi file untuk penyimpanan data");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file untuk penyimpanan data = ");
		String inputLokasiPenyimpanan =null;
		try{
			inputLokasiPenyimpanan = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		System.out.println("Masukkan lokasi file untuk menampilkan data");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file untuk menampilkan data = ");
		String inputLokasiTampil =null;
		try{
			inputLokasiTampil = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		try{
			File filePenyimpanan = new File(inputLokasiPenyimpanan);
			PrintWriter out = new PrintWriter(filePenyimpanan);
			File fileTampil = new File(inputLokasiTampil);
			PrintWriter sort = new PrintWriter(fileTampil);
		
		String pilihperulangan = null;
		int perulangan=0, batas = 0;
		
	do{
	mainMenu();
	String inputPilih = null;
	try{
		inputPilih = bufferedreader.readLine();
	}
	catch(IOException e){
		System.out.println("Input Error" + e.getMessage());
	}
	
	try{
		int pilih = Integer.parseInt(inputPilih);
		switch(pilih){
			case 1:
				String inputKode = null;
				String inputJenisBarang = null;
				String inputWarna = null;
				String inputJumlah = null;
				String inputHarga = null;
				int kode = 0, jumlah=0, harga=0;
				String jenisBarang =null, warna =null;
				boolean ulang=true;
				do{
					ulang = true;
				do{
					System.out.print("Kode = ");
					try{
						inputKode = bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Input Error" + error.getMessage());
					}
					try{
						kode = Integer.parseInt(inputKode);
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah meamsukkan format data, kode harus dalam bentuk angka");
						System.out.println("Silahkan masukkan kembali kode\n");
						continue;
					}
				}while(true);
				
				 for(int indeks=0;indeks<batas;indeks++){
						 if(kode==(int)arr[indeks][0]){
							 ulang=false;
							 System.out.println("Kode sudah digunakan");
							 System.out.println("SIlahkan masukkan kembali kode\n");
						}
				}
				}while(ulang==false);
				
				System.out.print("Jenis Barang = ");
					try{
						inputJenisBarang = bufferedreader.readLine();
						jenisBarang = inputJenisBarang;
					}
					catch(IOException error){
						System.out.println("Input Error" + error.getMessage());
					}
					
				System.out.print("Warna = ");
					try{
						inputWarna = bufferedreader.readLine();
						warna= inputWarna;
					}
					catch(IOException error){
						System.out.println("Input Error" + error.getMessage());
					}
					
				System.out.print("Jumlah = ");
					try{
						inputJumlah = bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Input Error" + error.getMessage());
					}
					try{
						jumlah = Integer.parseInt(inputJumlah);
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah memasukkan format data, jumlah harus dalam bentuk angka");
					}
					
				System.out.print("Harga = Rp ");
					try{
						inputHarga = bufferedreader.readLine();
					}
					catch(IOException error){
						System.out.println("Input Error" + error.getMessage());
					}
					try{
						harga = Integer.parseInt(inputHarga);
					}
					catch(NumberFormatException e){
						System.out.println("Anda salah meamsukkan format data, harga harus dalam bentuk angka");
					}
				
				String baris = "=========";
				
				arr[batas][0] = kode;
				arr[batas][1] = jenisBarang;
			    arr[batas][2] = warna;
			    arr[batas][3] = jumlah;
			    arr[batas][4] = harga;
				arr[batas][5] = baris;
				batas++;
				
				menulis menulisData = new menulis();
				menulisData.isiData(kode, jenisBarang, warna, jumlah, harga);
				menulisData.tampilkanData(inputLokasiPenyimpanan);
				break;
		
			case 2:
				System.out.println("Tampilkan Data barang berdasarkan");
				System.out.println("1. Kode");
				System.out.println("2. Jenis Barang");
				System.out.println("3. Warna");
				System.out.println("4. Jumlah");
				System.out.println("5. Harga");
				System.out.print("Masukkan Pilihan anda");
				String inputPilih1 = null;
				try{
					inputPilih1 = bufferedreader.readLine();
				}
				catch(IOException e){
					System.out.println("Input Error" + e.getMessage());
				}
				try{
					int pilih1 = Integer.parseInt(inputPilih1);
					switch(pilih1){
						case 1:
							String temp="";
							int tempInt=-1;
							for(int indeks=0;indeks<batas;indeks++){
								for(int indeksKedua=0;indeksKedua<batas;indeksKedua++){
									if( ( (int) arr[indeks][0] ) < ((int)arr[indeksKedua][0])){
										tempInt = (int) arr[indeksKedua][0];  
										arr[indeksKedua][0] = arr[indeks][0];  
										arr[indeks][0] = tempInt;
                                 
										temp = (String) arr[indeksKedua][1];  
										arr[indeksKedua][1] = arr[indeks][1];  
										arr[indeks][1] = temp;
                                 
										temp = (String) arr[indeksKedua][2];  
										arr[indeksKedua][2] = arr[indeks][2];  
										arr[indeks][2] = temp;
                                 
										tempInt = (int) arr[indeksKedua][3];  
										arr[indeksKedua][3] = arr[indeks][3];  
										arr[indeks][3] = tempInt;
                                 
										tempInt = (int) arr[indeksKedua][4];  
										arr[indeksKedua][4] = arr[indeks][4];  
										arr[indeks][4] = tempInt;
									}
								}
							}
							for(int indeks=0;indeks<batas;indeks++){
								for(int indeksKedua=0;indeksKedua<5;indeksKedua++){
									sort.println(arr[indeks][indeksKedua]);
								}
								sort.println("====");
							}
							sort.close();
							System.out.println("SortedInventory.txt succesfully modified");
							System.out.println("The .txt file can be found in the same folder this .java program located");
						break;
						
						case 2:
						temp="";
						for(int indeks=0;indeks<batas;indeks++){
							for(int indeksKedua=0;indeksKedua<batas;indeksKedua++){
								if(((String) arr[indeks][1]).compareToIgnoreCase((String) arr[indeksKedua][1]) < 0){
								tempInt = (int) arr[indeksKedua][0];  
                                arr[indeksKedua][0] = arr[indeks][0];  
                                arr[indeks][0] = tempInt;
                                
                                temp = (String) arr[indeksKedua][1];  
                                arr[indeksKedua][1] = arr[indeks][1];  
                                arr[indeks][1] = temp;
                                
                                temp = (String) arr[indeksKedua][2];  
                                arr[indeksKedua][2] = arr[indeks][2];  
                                arr[indeks][2] = temp;
                                
                                tempInt = (int) arr[indeksKedua][3];  
                                arr[indeksKedua][3] = arr[indeks][3];  
                                arr[indeks][3] = tempInt;
                                
                                tempInt = (int) arr[indeksKedua][4];  
                                arr[indeksKedua][4] = arr[indeks][4];  
                                arr[indeks][4] = tempInt;
								}
							}
						}
						for(int indeks=0;indeks<batas;indeks++){
							for(int indeksKedua=0;indeksKedua<5;indeksKedua++){
								sort.println(arr[indeks][indeksKedua]);
							}
							sort.println("====");
						}
						sort.close();
						System.out.println("SortedInventory.txt succesfully modified");
						System.out.println("The .txt file can be found in the same folder this .java program located");
						break;
						
						case 3:
						temp="";
						for(int indeks=0;indeks<batas;indeks++){
							for(int indeksKedua=0;indeksKedua<batas;indeksKedua++){
								if(((String) arr[indeks][2]).compareToIgnoreCase((String) arr[indeksKedua][2]) < 0){
								tempInt = (int) arr[indeksKedua][0];  
                                arr[indeksKedua][0] = arr[indeks][0];  
                                arr[indeks][0] = tempInt;
                                
                                temp = (String) arr[indeksKedua][1];  
                                arr[indeksKedua][1] = arr[indeks][1];  
                                arr[indeks][1] = temp;
                                
                                temp = (String) arr[indeksKedua][2];  
                                arr[indeksKedua][2] = arr[indeks][2];  
                                arr[indeks][2] = temp;
                                
                                tempInt = (int) arr[indeksKedua][3];  
                                arr[indeksKedua][3] = arr[indeks][3];  
                                arr[indeks][3] = tempInt;
                                
                                tempInt = (int) arr[indeksKedua][4];  
                                arr[indeksKedua][4] = arr[indeks][4];  
                                arr[indeks][4] = tempInt;
								}
							}
						}
						for(int indeks=0;indeks<batas;indeks++){
							for(int indeksKedua=0;indeksKedua<5;indeksKedua++){
								sort.println(arr[indeks][indeksKedua]);
							}
							sort.println("====");
						}
						sort.close();
						System.out.println("SortedInventory.txt succesfully modified");
						System.out.println("The .txt file can be found in the same folder this .java program located");
					break;
				
					case 4:
					temp="";
					for(int indeks=0;indeks<batas;indeks++){
						for(int indeksKedua=0;indeksKedua<batas;indeksKedua++){
							if( ( (int) arr[indeks][3] ) < ((int)arr[indeksKedua][3])){
								tempInt = (int) arr[indeksKedua][0];  
                                arr[indeksKedua][0] = arr[indeks][0];  
                                arr[indeks][0] = tempInt;
                                
                                temp = (String) arr[indeksKedua][1];  
                                arr[indeksKedua][1] = arr[indeks][1];  
                                arr[indeks][1] = temp;
                                
                                temp = (String) arr[indeksKedua][2];  
                                arr[indeksKedua][2] = arr[indeks][2];  
                                arr[indeks][2] = temp;
                                
                                tempInt = (int) arr[indeksKedua][3];  
                                arr[indeksKedua][3] = arr[indeks][3];  
                                arr[indeks][3] = tempInt;
                                
                                tempInt = (int) arr[indeksKedua][4];  
                                arr[indeksKedua][4] = arr[indeks][4];  
                                arr[indeks][4] = tempInt;
							}
						}
					}
					for(int indeks=0;indeks<batas;indeks++){
						for(int indeksKedua=0;indeksKedua<5;indeksKedua++){
							sort.println(arr[indeks][indeksKedua]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt succesfully modified");
					System.out.println("The .txt file can be found in the same folder this .java program located");
					break;
					
					case 5:
					temp="";
					for(int indeks=0;indeks<batas;indeks++){
						for(int indeksKedua=0;indeksKedua<batas;indeksKedua++){
							if( ( (int) arr[indeks][4] ) < ((int)arr[indeksKedua][4])){
								tempInt = (int) arr[indeksKedua][0];  
                                arr[indeksKedua][0] = arr[indeks][0];  
                                arr[indeks][0] = tempInt;
                                
                                temp = (String) arr[indeksKedua][1];  
                                arr[indeksKedua][1] = arr[indeks][1];  
                                arr[indeks][1] = temp;
                                
                                temp = (String) arr[indeksKedua][2];  
                                arr[indeksKedua][2] = arr[indeks][2];  
                                arr[indeks][2] = temp;
                                
                                tempInt = (int) arr[indeksKedua][3];  
                                arr[indeksKedua][3] = arr[indeks][3];  
                                arr[indeks][3] = tempInt;
                                
                                tempInt = (int) arr[indeksKedua][4];  
                                arr[indeksKedua][4] = arr[indeks][4];  
                                arr[indeks][4] = tempInt;
							}
						}
						
					}
					for(int indeks=0;indeks<batas;indeks++){
						for(int indeksKedua=0;indeksKedua<5;indeksKedua++){
							sort.println(arr[indeks][indeksKedua]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt succesfully modified");
					System.out.println("The .txt file can be found in the same folder this .java program located");
					break;
					default:
						System.out.println("Anda salah memasukkan pilihan");
					break;
					}
				
				out.close();
				System.out.println("Inventory.txt succesfully modified");
				System.out.println("The .txt file can be found in the same folder this .java program located");
				break;	
				}
				catch (NumberFormatException e){
					System.out.println("Anda salah memasukkan format data, pilihan diinput dalam bentuk angka");
				}
				break;
			default:
				System.out.println("Anda salah memasukkan pilihan");
				break;
		}
	}
	catch(NumberFormatException e){
		System.out.println("Anda salah memasukkan format data");
	}
	System.out.println("Apakah Anda ingin mengulangi proses?");
	System.out.println("1. yes 2. no");
	System.out.print("Masukkan pilihan anda = ");
	    try{
            pilihperulangan= bufferedreader.readLine();
            try{
				perulangan = Integer.parseInt(pilihperulangan);
                if (perulangan ==1) {
                    perulangan = 1;
                }
                else if (perulangan == 2){
                    perulangan = 0;
                }
                else
                    System.out.println("Anda salah memasukkan pilihan");
            }
			catch(NumberFormatException e){
                System.out.println("Anda salah memasukkan format data");
            }
        }
	    catch(IOException error){
			System.out.println("salah memasukkan pilihan"+error.getMessage());
	    }
	} while(perulangan==1);
		
		}
		catch(FileNotFoundException e){
			System.out.println("Tidak dapat menemukan likasi yang anda maksud");
		}
		
	}
}